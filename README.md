Performing the extreme value analysis on Hilbert phases extracted from
grid data.


The actual analysis can be found in the [public](public/) directory. I
generated a HTML document containing all the figures and
descriptions. It is best viewed using
[Chromium](https://www.chromium.org/). (Using Firefox you might have
some issues printing the document).

# [data](data/)
Contains the fitted GEV parameters for each observation site order in
the same way as in the ERA-Interim data set. It is stored as standard
.csv files. 

- ERA-daily-anom-gev-max.csv - The three GEV parameters fitted to the
  annual block maxima of the temperature anomalies
- ERA-daily-anom-gev-min.csv - The three GEV parameters fitted to the
  annual block minima of the temperature anomalies
- ERA-daily-phases-gev-max.csv - The three GEV parameters fitted to the
  annual block maxima of the instantaneous phases
- ERA-daily-phases-gev-min.csv - The three GEV parameters fitted to the
  annual block minima of the instantaneous phases

# [figures](figures/)

A copy of the figures generated during the analysis can be found in
the [figures](figures/) folder.
